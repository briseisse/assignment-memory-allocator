//
// Created by brune on 10/03/2022.
//

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <sys/mman.h>


#ifndef MAP_FIXED
#define MAP_FIXED 0x100000
#endif

void debug(const char *fmt, ...);

static struct block_header* get_block_header (void *data){
    return (struct block_header*)(((uint8_t*)data)-offsetof(struct block_header, contents));
}
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd );

static void* block_after(struct block_header const* block);
static inline void* heap_last(struct block_header* first);
static void* map_pages(void const *addr, size_t length, int additional_flags);

void* heap;

//heap initialisation
bool test1 (){
    debug("Heap initialisation... \n");
    debug("Testing...\n");
    debug("Test 1/6 \n");
    debug("Test started... \n");
    const size_t INITIAL_HEAP_SIZE = 1000;
    heap = heap_init(INITIAL_HEAP_SIZE);
    if(heap == NULL){
        err("Test 1 failed. Heap allocator is not working! \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Success! Test 1/6 passed. \n");
    return true;
}

//Allocating and freeing a block
bool test2() {
    debug("Test 2/6 \n");
    debug("Test started... \n");
    const size_t test_size = 100;
    void* block1 = _malloc(test_size);
    if(!block1){
        err("Error! Block allocator is not working \n");
        return false;
    }
    debug_heap(stdout,heap);
    _free(block1);
    struct block_header* header = get_block_header(block1);
    if(!(header->is_free)){
        err("Test 2 failed. Block freer is not working properly.\n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Success! Test 2/6 passed \n");
    return true;
}

//Allocating and freeing multiple blocks
bool test3 (){
    debug("Test 3/6 \n");
    debug("Test started... \n");
    const size_t test_size = 100;
    void* block1 = _malloc(test_size);
    void* block2 = _malloc(test_size);
    debug_heap(stdout,heap);
    if(!block1 || !block2){
        err("Test 3 failed. Block initializer is not working properly. \n");
        return false;
    }
    _free(block1);
    struct block_header* header1 = get_block_header(block1);
    struct block_header* header2 = get_block_header(block2);
    if(!header1->is_free && header2->is_free){
        err("Test 3 failed. Block freer is not working properly. \n");
        return false;
    }
    _free(block2);
    if(!header1->is_free || !header2->is_free){
        err("Test 3 failed. Block freer is not working properly.\n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Succes! Test 3/6 passed. \n");
    return true;
}

//adjacency check
bool test4(){
    debug("Test 4/6 \n");
    debug("Test started... \n");
    const size_t test_size = 100;
    void* block1 = _malloc (test_size);
    void* block2 = _malloc(test_size);
    void* block3 = _malloc(test_size);
    if(!block1 || !block2 || !block3){
        err("Error! Block returns null \n");
        return false;
    }
    debug_heap(stdout, heap);
    struct block_header* header1 = get_block_header(block1);
    struct block_header* header2 = get_block_header(block2);
    struct block_header* header3 = get_block_header(block3);
    if(!blocks_continuous(header1, header2) || !blocks_continuous(header2, header3)){
        err("Test 4 failed! Blocks are not adjacent. \n");
        return false;
    }
    debug_heap(stdout, heap);
    _free(block1);
    _free(block2);
    _free(block3);
    debug("Success! Test 4 passed. \n");
    return true;
}
//New region
bool test5(){
    debug("Test 5/6 \n");
    debug("Test started... \n");
    debug_heap(stdout, heap);
    void* block1 = _malloc (10000);
    struct block_header* header1 = get_block_header(block1);
    void* last = heap_last(header1);
    map_pages(last, 1000, MAP_FIXED);
    debug_heap (stdout, heap);
    void* block2 = _malloc(10000);
    if(blocks_continuous(last, get_block_header(block2))) {
        err("Error: Blocks are not adjacent \n");
        return false;
    }
    debug_heap(stdout,heap);
    debug("Succes! Test 5/6 passed.");
    _free(block1);
    _free(block2);
    return true;
}
//heap growth check
bool test6(){
    printf("Test 6/6 \n");
    printf("Test started... \n");
    void* block1 = _malloc(10000);
    if(!block1){
        err("Test 6 failed. Heap growth failed. \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Success! Test 6 passed. \n");
    return true;
}

bool tests_success(){
    if(test1()){
        bool check = test2() && test3() && test4() && test5() && test6();
        check == true ? debug("Congratulations! All tests passes! \n") : debug("Some tests failed. Please, try again. \n");
        return check;
    } else return false;
}

static void* map_pages(void const *addr, size_t length, int additional_flags){
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static inline void* heap_last (struct block_header *first){
    while(first->next != NULL) first = first->next;
    return (void*)block_after(first);
}

static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}


static void* block_after(struct block_header const* block){
    return (void*)(block->contents + block->capacity.bytes);
}

int main(){
    if(tests_success()){
        printf("Ouuraaa! Success... \n");
        return 0;
    }
    return 1;
}
